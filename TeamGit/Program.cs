﻿using System;

namespace TeamGit
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int totalNumber = 0;

            Console.Write("Please enter a minimum number: ");
            int mimNum = Convert.ToInt32(Console.ReadLine());

            Console.Write("Please enter a maximum number: ");
            int maxNum = Convert.ToInt32(Console.ReadLine());

            for (int i = mimNum; i <= maxNum; i++)
            {
                totalNumber += i;
                Console.Write(i + " ");
            }

            Console.WriteLine("The sum is: " + totalNumber);
        }
    }
}
